var $ = jQuery;
$(document).ready(function(){
    $("#formulario_ikano").validate({
        errorElement: 'small',
        rules: {
            nombre_form: { required: true, minlength: 3},
            empresa_form: { required: true, minlength: 4},
            //pais_form: { required: true, minlength: 3},
            departamento_form: { required: true},
            ciudad_form: { required: true},
            correo_form: { required: true, minlength: 4, email: true},
            comentarios_form: { minlength: 2},
            autorizacion: { required:true},
        },

        messages: {

            nombre_form: "Debe introducir sus nombres y apellidos completos.",
            empresa_form: "Debe escribir el nombre de la empresa.",
            //pais_form: "Debe escribir el nombre del país donde se encuentra.",
            departamento_form: "Debe elegir un departamento.",
            ciudad_form: "Debe elegir una ciudad.",
            correo_form: "Debe escribir un correo electrónico válido.",
            comentarios_form: "Debe escribir algún comentario.",
            autorizacion: "Este campo es requerido.",

        },
        submitHandler: function(form){

            var nombre_form = document.getElementById('nombre_form').value;
            var empresa_form = document.getElementById('empresa_form').value;
            //var pais_form = document.getElementById('pais_form').value;
            var departamento_form = document.getElementById('departamento_form').value;
            var ciudad_form = document.getElementById('ciudad_form').value;
            var correo_form = document.getElementById('correo_form').value;
            var comentarios_form = document.getElementById('comentarios_form').value;
            var autorizacion = document.getElementById('autorizacion').value;

            $.ajax({
                type: "POST",
                url: "send.php",
                data: {nombre_form:nombre_form, empresa_form:empresa_form, departamento_form:departamento_form, ciudad_form:ciudad_form, correo_form:correo_form, comentarios_form:comentarios_form, autorizacion:autorizacion},
                // pais_form:pais_form
                success: function(data){
                    // $('.cd-popup').addClass('is-visible');
                    /*$("#ok").html(data);*/
                    $(".spin-load").hide();
                    window.location.href = "http://ikanoconnect.vectorial.co/gracias.php";
                    // $("#ok").show();
                    // $("#formid").hide();

                }
            });
        }
    });


});
