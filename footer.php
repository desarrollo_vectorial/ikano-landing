<footer>
    <section id="footer">
        <div class="container last-btn">
            <div class="row">
                <div class="col-md-12 text-center padding-tb-50">
                    <a href="#nogo" class="btn btn-yellow btn-round active_form">
                        solicitar información
                    </a>
                </div>
            </div>
        </div>
        <div class="container-fluid copy">
            <div class="copyposition">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-7">
                            <p>Copyright todos los derechos reservados - Ikano Connect 2018</p>
                        </div>
                        <div class="col-md-5">
                            <span>Una solución: <a href="http://www.vectorial.co" target="_blank"><img src="/assests/images/logo-vectorial.svg" alt="Logo Vectorial"></a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>

<video poster="assests/images/poster.png" id="bgvid" playsinline autoplay muted loop>
    <source src="assests/videos/3.mp4" type="video/mp4">
</video>
<!-- JQuery -->
<script type="text/javascript" src="assests/js/jquery-3.1.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assests/js/bootstrap.min.js"></script>
<script src="https://rawgit.com/Olical/EventEmitter/master/EventEmitter.min.js"></script>
<script src="assests/js/owl.carousel.min.js"></script>
<script src="assests/js/moment.min.js"></script>
<script src="assests/js/bootstrap-datetimepicker.js"></script>
<script src="assests/js/bootstrap-selectpicker.js"></script>
<script src="assests/js/bootstrap-tagsinput.js"></script>
<script src="assests/js/material.min.js"></script>
<script src="assests/js/material-kit.js"></script>
<script src="assests/js/nouislider.min.js"></script>
<script src="assests/js/smartscroll.min.js"></script>
<script src="assests/js/jquery.easy-autocomplete.min.js"></script>
<script src="assests/js/my-functions.js"></script>
<script src="assests/js/departamentos.js"></script>
<script src="assests/js/jquery.validate.js"></script> <!-- JQuery Validate -->
<script src="assests/js/validate.js"></script> <!-- ValidateForm -->
<!--<script src="assests/js/niceselect/jquery.nice-select.js"></script>Nice Select-->
</body>
</html>