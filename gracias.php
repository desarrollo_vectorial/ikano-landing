<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ikano</title>

    <!-- Bootstrap -->
    <link href="/assests/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assests/css/main.css" rel="stylesheet">
    <link href="/assests/css/owl.carousel.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/png" href="/assests/images/favicon.png"/>
</head>
<body>
<video poster="assests/images/poster.png" id="bgvid" playsinline autoplay muted loop>
    <source src="assests/videos/3.mp4" type="video/mp4">
</video>
<div class="section-wrapper">
    <header>
        <section id="header" class="header section" data-hash="header">
            <div class="container-fluid">
                <nav class="navbar navbar-transparent">
                    <div class="container-fluid">
                        <div class="col-md-12 navbar-header">
                            <a class="navbar-brand" href="/">
                                <img src="assests/images/logo.svg" alt="Logo Ikano">
                            </a>
                            <ul class="nav navbar-nav navbar-right rrss">
                                <li>
                                    <a href="#facebook">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#youtube">
                                        <i class="fa fa-youtube-play"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#instagram">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="message-title-thankyou text-center">
                    <p><span>¡Muchas gracias!, hemos recibido tus datos,</span><br>en los próximos días nuestro equipo se pondrá en contacto para</br>resolver tu solicitud.</h2> <br></p>
                    <a href="http://ikanoconnect.vectorial.co/" class="btn btn-yellow btn-round active_form">
                        Volver a Ikano Connect
                    </a>
                </div>

            </div>
        </section>
    </header>
</div>
<!-- JQuery -->
<script type="text/javascript" src="assests/js/jquery-3.1.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assests/js/bootstrap.min.js"></script>
<script src="https://rawgit.com/Olical/EventEmitter/master/EventEmitter.min.js"></script>
<script src="assests/js/owl.carousel.min.js"></script>
<script src="assests/js/moment.min.js"></script>
<script src="assests/js/bootstrap-datetimepicker.js"></script>
<script src="assests/js/bootstrap-selectpicker.js"></script>
<script src="assests/js/bootstrap-tagsinput.js"></script>
<script src="assests/js/material.min.js"></script>
<script src="assests/js/material-kit.js"></script>
<script src="assests/js/nouislider.min.js"></script>
<script src="assests/js/smartscroll.min.js"></script>
<script src="assests/js/my-functions.js"></script>
<script src="assests/js/departamentos.js"></script>
<script src="assests/js/jquery.validate.js"></script> <!-- JQuery Validate -->
<script src="assests/js/validate.js"></script> <!-- ValidateForm -->
<!--<script src="assests/js/niceselect/jquery.nice-select.js"></script>Nice Select-->
</body>
</html>



