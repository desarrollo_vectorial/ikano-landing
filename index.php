<?php include dirname(__FILE__) .  '/head.php'; ?>
<?php include dirname(__FILE__) .  '/contact_form.php'; ?>
    <div class="section-wrapper">
        <header>
            <section id="header" class="header section" data-hash="header">
                <div class="container-fluid">
                    <nav class="navbar navbar-fixed-top">
                        <div class="container-fluid">
                            <div class="col-md-12 navbar-header">
                                <a class="navbar-brand" href="/">
                                    <img src="assests/images/logo.svg" alt="Logo Ikano">
                                </a>
                                <ul class="nav navbar-nav navbar-right rrss">
                                    <li>
                                        <a href="#nogo" class="btn btn-yellow btn-round active_form">
                                            solicitar información
                                        </a>
                                    </li>
<!--                                    <li>-->
<!--                                        <a href="#facebook">-->
<!--                                            <i class="fa fa-facebook"></i>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <a href="#youtube">-->
<!--                                            <i class="fa fa-youtube-play"></i>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <a href="#twitter">-->
<!--                                            <i class="fa fa-twitter"></i>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <a href="#instagram">-->
<!--                                            <i class="fa fa-instagram"></i>-->
<!--                                        </a>-->
<!--                                    </li>-->
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <div class="header-text-title text-center">
                        <p>LA FORMA DE LLEVAR LA INTRANET AL <br><span>100% DE SU ORGANIZACIÓN</span></p>
                    </div>
                    <div class="scrolldownarrow text-center">
                        <a href="/#ikano" class="hvr-wobble-vertical">
                            <img src="/assests/images/scrollimg.png" alt="">
                            <br>
                            <small>VER MÁS</small>
                        </a>
                    </div>
                </div>
            </section>
        </header>

        <section id="ikano">
            <div class="ikano section" data-hash="ikano">
                <div class="ikanosection">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">

                                <p>Uno de los mayores dolores de cabeza de las organizaciones es llevar la intranet a todos sus empleados, pues está demostrado que <strong><span>sólo el 56% de las personas realmente utilizan la intranet</span></strong> porque no pueden acceder a ella desde su celular.</p>

                                <p>Es por eso que nace <strong><span>Ikano Connect,</span></strong> una aplicación móvil que permite que todos tus empleados accedan a las funcionalidades principales de la intranet desde su celular.</p>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="scrolldownarrow text-center">
                    <a href="/#servicios" class="hvr-wobble-vertical">
                        <img src="/assests/images/arrow2.png" alt="">
                        <br>
                        <small>VER MÁS</small>
                    </a>
                </div>
            </div>
        </section>

        <section id="servicios">
            <div class="servicios section" data-hash="servicios">
                <div class="serviciossection">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="col-md-12">
                                    <p class="title-servicos">DESDE<strong> IKANO CONNECT</strong> PODRÁ INGRESAR A:</p>
                                </div>

                                <ul>
                                    <li>
                                        <div class="btn-servicios">
                                            <img src="/assests/images/1.png" alt="Noticias">
                                            <p>Noticias</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="btn-servicios">
                                            <img src="/assests/images/2.png" alt="Solicitud de vacaciones">
                                            <p>Solicitud<br>de vacaciones</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="btn-servicios">
                                            <img src="/assests/images/3.png" alt="Directorio telefónico">
                                            <p>Directorio<br>telefónico</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="btn-servicios">
                                            <img src="/assests/images/4.png" alt="Beneficios">
                                            <p>Beneficios</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="btn-servicios">
                                            <img src="/assests/images/5.png" alt="Eventos">
                                            <p>Eventos</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="btn-servicios">
                                            <img src="/assests/images/6.png" alt="Menú del restaurante">
                                            <p>Menú del<br>restaurante</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="btn-servicios">
                                            <img src="/assests/images/7.png" alt="Desprendible de nómina">
                                            <p>Desprendible<br>de nómina</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="btn-servicios">
                                            <img src="/assests/images/8.png" alt="Incapacidades">
                                            <p>Incapacidades</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="btn-servicios">
                                            <img src="/assests/images/9.png" alt="Refrigerios">
                                            <p>Refrigerios</p>
                                        </div>
                                    </li>
                                </ul>

                                <div class="col-md-12">
                                    <p class="text-servicios">Y otras funcionalidades que su organización decida.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="scrolldownarrow text-center">
                    <a href="/#permite" class="hvr-wobble-vertical">
                        <img src="/assests/images/scrollimg.png" alt="">
                        <br>
                        <small>VER MÁS</small>
                    </a>
                </div>
            </div>
        </section>

        <section id="permite">
            <div class="permite section" data-hash="permite">
                <div class="permitesection">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="fixed-content">
                                <p class="text-permite">Usar Ikano Connect es muy fácil, el colaborador sólo debe descargar la aplicación en <strong>Google Play o App Store,</strong> iniciar sesión y encontrará todo un mundo de posibilidades para conectarse con su trabajo sin que su organización ponga en riesgo la seguridad de la información</p>
                                <p class="title-permite">Ikano Connect permite:</p>
                            </div>



                            <div class="owl-carousel owl-theme slider-permite">
                                <div class="item slide1">
                                    <div class="no-padding content-slide">
                                        <div class="col-md-12 img-content-slide">
                                            <img src="/assests/images/icono-permite1.svg" class="img-responsive">
                                        </div>
                                        <div class="col-md-12">
                                            <p>Configurar los aplicativos que presten un servicio al colaborador: desprendibles de nómina, beneficios, solicitud de vacaciones, aunque estos no estén nativamente en la intranet.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item slide2">
                                    <div class="no-padding content-slide">
                                        <div class="col-md-12 img-content-slide">
                                            <img src="/assests/images/2.svg" class="img-responsive">
                                        </div>
                                        <div class="col-md-12">
                                            <p>Personalizar la aplicación con el look and feel de su compañía.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item slide3">
                                    <div class="no-padding content-slide">
                                        <div class="col-md-12 img-content-slide">
                                            <img src="/assests/images/3.svg" class="img-responsive">
                                        </div>
                                        <div class="col-md-12">
                                            <p>Todos sus empleados podrán ingresar a la aplicación, aunque no tengan un usuario o correo electrónico de la compañía.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item slide4">
                                    <div class="no-padding content-slide">
                                        <div class="col-md-12 img-content-slide">
                                            <img src="/assests/images/4.svg" class="img-responsive">
                                        </div>
                                        <div class="col-md-12">
                                            <p>Tener el perfil del empleado: nombre, foto, cargo.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item slide5">
                                    <div class="no-padding content-slide">
                                        <div class="col-md-12 img-content-slide">
                                            <img src="/assests/images/5.svg" class="img-responsive">
                                        </div>
                                        <div class="col-md-12">
                                            <p>Tener estadísticas del uso del aplicativo y sus funcionalidades.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

<?php include dirname(__FILE__) .  '/footer.php'; ?>