var $ = jQuery;

$.smartscroll({
    sectionClass: "section",
    headerHash: "Bienvenido-a-IKANO"
});

$('.owl-carousel').owlCarousel({
    autoplay: true,
    autoplayTimeout: 10000,
    loop:true,
    margin:10,
    nav:true,
    mouseDrag:false,
    pullDrag:false,
    navText:["<i class='fa fa-caret-left' aria-hidden='true'></i>","<i class='fa fa-caret-right' aria-hidden='true'></i>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:3
        }
    }
});

// Add smooth scrolling on all links inside the navbar
$(".scrolldownarrow a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {

        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $("html,body").animate(
            { scrollTop: $(hash).offset().top},
            1000
        );
        return false;
        e.preventDefault();

    } // End if

});

$(".active_form").on('click', function () {
    $(".contact_form").toggleClass('toggle_form');

});
$("#close_form").on('click',function () {
    $(".contact_form").toggleClass('toggle_form');
});
$('button[type=submit]').on('click',function () {
    $('.spin-load').show();
});

var options = {
    url: "/assests/js/countries.json",
    getValue: "name",
    list: {
        maxNumberOfElements: 10,
        match: {
            enabled: true
        }
    }
};
$("#pais_form").easyAutocomplete(options);
